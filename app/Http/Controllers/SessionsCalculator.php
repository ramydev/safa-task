<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ScheduledSessionsRequest;

class SessionsCalculator extends Controller
{
    private $startDate;
    private $endDate;
    private $daysPerWeek=[];
    private $sessionsToFinishChapter=0;
    private $sessionsToFinishAllChapters=0;
    private $chapters=30;
    private $week = [0=>6,1=>0,2=>1,3=>2,4=>3,5=>4,6=>5];


    /**
     * Return Scheduled session/days to 
     * complete chapters 
     * @param Request $request
     */
    public function scheduledSessions(ScheduledSessionsRequest $request){
        //get days per week when saturdays is the begining of week
        $this->daysPerWeek = array_values(
                array_intersect_key($this->week,array_flip($request->days_per_week))
                );
        $this->sessionsToFinishChapter = $request['sessions_to_finish_chapter'];
        //get sessions count to finish all chapters
        $this->sessionsToFinishAllChapters = $this->sessionsToFinishChapter*$this->chapters;
        //get Weeks
        $Weeks = $this->sessionsToFinishAllChapters/count($this->daysPerWeek);
        //get startDate and endDate
        $this->startDate = new \DateTime($request['start_date']);
        $this->endDate = new \DateTime($request['start_date']);
        //get End dade by Add weeks to start date , then add 1 day to last date when iterating 
        $this->endDate=$this->endDate->modify('+'.$Weeks.' week')->modify('+1 day');
        $interval = \DateInterval::createFromDateString('1 day');
        //dd($this->startDate,$this->endDate);
        $period = new \DatePeriod($this->startDate,$interval, $this->endDate);
        $sessionsArray = [];
        //foreach persiod to get Sessions week days
        foreach($period as $day){
            if(in_array($day->format("w"),$this->daysPerWeek)){
                $sessionsArray[] =  $day->format("Y-m-d");
            }
        }
        return response()->json($sessionsArray);
    }
}
