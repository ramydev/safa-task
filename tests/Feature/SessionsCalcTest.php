<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SessionsCalcTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_session_calc()
    {
        $response = $this->post('/api/sessions-calculator', [
            'start_date'=>'2021-01-21',
            'days_per_week'=>[1,4,6],
            'sessions_to_finish_chapter'=>1
        ]);
        $responseValues = ["2021-01-22","2021-01-24",
            "2021-01-27","2021-01-29","2021-01-31",
            "2021-02-03","2021-02-05","2021-02-07",
            "2021-02-10","2021-02-12","2021-02-14",
            "2021-02-17","2021-02-19","2021-02-21",
            "2021-02-24","2021-02-26","2021-02-28",
            "2021-03-03","2021-03-05","2021-03-07",
            "2021-03-10","2021-03-12","2021-03-14",
            "2021-03-17","2021-03-19","2021-03-21",
            "2021-03-24","2021-03-26","2021-03-28","2021-03-31"];
        $response->assertJson($responseValues);
        $response->assertStatus(200);
    }
    
    public function test_session_calc_valide_date(){
        $response = $this->post('/api/sessions-calculator', [
            'days_per_week'=>[1,4,6],
            'sessions_to_finish_chapter'=>1
        ]);
        $response->assertJson(["start_date"=> ["The start date field is required."]]);
        $response->assertStatus(422);
    }
    
    public function test_session_calc_validate_days_per_week(){
        $response = $this->post('/api/sessions-calculator', [
            'start_date'=>'2021-01-21',
            'sessions_to_finish_chapter'=>1
        ]);
        $response->assertJson([ "days_per_week"=> ["The days per week field is required."]]);
        $response->assertStatus(422);
    }
}
